# Gigas' Configurations for Counter-Strike 2
![preview](img/gigas-cs2cfg-preview.png)

## How to use
1. [**Download**](https://gitlab.com/Gigas/csgo-config/-/archive/master/csgo-config-master.zip) the zipped config files.
2. Extract the **cfg** folder to your cs2 game path, typically
   - `C:\Program Files (x86)\Steam\steamapps\common\Counter-Strike Global Offensive\game\csgo\` [^notice]
3. Replace files if prompted.
4. Add `+exec gigas.cfg` to your launch options.

### Correct Placement
![correctplacement](img/correctplacement.png)

[^notice]: Notice the **game\csgo**
